
package org.r3ntryde.data.dto;

import java.util.HashMap;
import java.util.UUID;


public class VehicleData extends DataTransferObject {

    private String serial;
    private String model;
    private String color;
    private int prodYear;
    private HashMap<UUID, BookingData> bookings = new HashMap<>();

    public VehicleData(String serial, String model, String color, int prodYear, UUID id) {
        super(id);
        this.serial = serial;
        this.model = model;
        this.color = color;
        this.prodYear = prodYear;
    }

    public String getSerial() {
        return serial;
    }
    public String getModel() {
        return model;
    }
    public String getColor() { return color; }
    public int getProdYear() { return prodYear; }
    public HashMap<UUID, BookingData> getBookings() { return bookings; }
    public void addBooking(BookingData booking) {
        bookings.put(booking.getID(), booking);
    }
    public void removeBooking(UUID id) {
        bookings.remove(id);
    }


}

