package org.r3ntryde.data.dto;


import java.util.UUID;

public class AdminData extends AccountData {

    public AdminData(String name, String pass, UUID id) {
        super(id, name, pass);
    }

}
