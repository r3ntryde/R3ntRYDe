package org.r3ntryde.data.dto;

import java.util.UUID;

public abstract class AccountData extends DataTransferObject {

    private String accountName;
    private String password;

    public AccountData(UUID id, String accountName, String password) {
        super(id);
        this.accountName = accountName;
        this.password = password;
    }

    public String getAccountName() {
        return accountName;
    }
    public String getPass() {
        return password;
    }
}
