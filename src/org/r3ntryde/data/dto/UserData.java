package org.r3ntryde.data.dto;

import java.util.HashMap;
import java.util.UUID;

public class UserData extends AccountData {


    private String fullName;
    private String adress;
    private HashMap<UUID, BookingData> bookings = new HashMap<>();

    public UserData(String accountName, String password, String fullName, String adress, UUID id) {
        super(id, accountName, password);
        this.fullName = fullName;
        this.adress = adress;
    }

    public String getFullName() { return fullName;}
    public String getAdress() {return adress;}

    public HashMap<UUID, BookingData> getBookings() {
        return bookings;
    }
    public void addBooking(BookingData booking) {
        bookings.put(booking.getID(), booking);
    }
    public void removeBooking(UUID id) {
       bookings.remove(id);
    }

}
