package org.r3ntryde.data.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

public abstract class DataTransferObject implements Serializable {

    private UUID id;

    public DataTransferObject(UUID id) {
        this.id = id;
    }
    public UUID getID() {
        return id;
    }




}
