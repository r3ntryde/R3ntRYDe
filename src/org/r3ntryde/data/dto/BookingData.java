

package org.r3ntryde.data.dto;

import java.time.LocalDate;
import java.util.UUID;


public class BookingData extends DataTransferObject {

    private LocalDate start;
    private int duration;
    private UUID vehicleID;

    public BookingData(LocalDate start, int duration, UUID id, UUID vehicleID) {
        super(id);
        this.start = start;
        this.duration = duration;
        this.vehicleID = vehicleID;
    }

    public LocalDate getStart() {
        return start;
    }
    public int getDuration() {
        return duration;
    }
    public UUID getVehicleID() {
        return vehicleID;
    }
}



