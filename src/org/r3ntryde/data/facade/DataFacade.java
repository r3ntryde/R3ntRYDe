
package org.r3ntryde.data.facade;

import org.r3ntryde.data.dto.*;
import org.r3ntryde.domain.factory.DTOFactory;
import org.r3ntryde.data.persistance.facade.IPersistanceFacade;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Observable;
import java.util.UUID;

//Sköter all hantering inom datalagret och hanterar kommunikationen till persistance-delen av datalagret.

public class DataFacade extends Observable implements IDataFacade {

    private IPersistanceFacade persistance;

    public DataFacade(IPersistanceFacade persistance) {
        this.persistance = persistance;
    }


    @Override
    public void saveObject(DataTransferObject dto) {
        persistance.saveObject(dto);
    }

    @Override
    public DataTransferObject loadObject(DataTransferObject dto) {
        return persistance.loadObject(dto);
    }

    @Override
    public ArrayList<DataTransferObject> loadAllObjects(DataTransferObject dto) {
        return persistance.loadAllObjects(dto);
    }

    @Override
    public void deleteObject(DataTransferObject dto) {
        persistance.deleteObject(dto);
    }

    @Override
    public void resetSave(DataTransferObject dto) {
        persistance.resetSave(dto);
    }


}

