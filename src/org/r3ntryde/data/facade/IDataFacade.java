
package org.r3ntryde.data.facade;


import org.r3ntryde.data.dto.DataTransferObject;

import java.util.ArrayList;

public interface IDataFacade {


    void saveObject(DataTransferObject dto);
    DataTransferObject loadObject(DataTransferObject dto);
    ArrayList<DataTransferObject> loadAllObjects(DataTransferObject dto);
    void deleteObject(DataTransferObject dto);

    void resetSave(DataTransferObject dto);

}

