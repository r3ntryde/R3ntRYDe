package org.r3ntryde.data.persistance.brokers;


import org.r3ntryde.data.dto.DataTransferObject;
import org.r3ntryde.data.dto.VehicleData;
import java.io.*;
import java.util.ArrayList;


public class VehicleBroker extends FileBroker {


    private static File saveFile = new File("savedVehicles.ser");

    public VehicleBroker() {

        try {
            saveFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public DataTransferObject findInStorage(DataTransferObject dto) {

        VehicleData dataToLoad = (VehicleData) dto;
        for (DataTransferObject o : loadFile()) {
            VehicleData loadedData = (VehicleData) o;
            if (loadedData.getID().equals(dto.getID()) || loadedData.getSerial().equals(dataToLoad.getSerial())) {
                return loadedData;
            }
        }

        return null;
    }

    public void saveIntoStorage(DataTransferObject dto) {

        deleteFromStorage(dto);
        ArrayList<DataTransferObject> allVehicles = loadFile();
        allVehicles.add(dto);
        saveFile(allVehicles);
    }

    public void deleteFromStorage(DataTransferObject dto) {

        ArrayList<DataTransferObject> allVehicles = loadFile();
        DataTransferObject vehicleToDelete = null;
        for (DataTransferObject o : allVehicles) {
            if (o.getID().equals(dto.getID())) {
                vehicleToDelete = o;
            }
        }
        allVehicles.remove(vehicleToDelete);
        saveFile(allVehicles);

    }

    public void resetSaveFile(DataTransferObject dto) {

        ArrayList<DataTransferObject> allVehicles = new ArrayList<>();
        saveFile.delete();
        saveFile(allVehicles);
    }

    public ArrayList<DataTransferObject> loadFile() {

        try {
            FileInputStream fis = new FileInputStream(saveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<DataTransferObject> allVehicles = (ArrayList<DataTransferObject>) ois.readObject();
            ois.close();
            return allVehicles;


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveFile(ArrayList<DataTransferObject> objectsToSave) {

        try {
            FileOutputStream fos = new FileOutputStream(saveFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(objectsToSave);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
