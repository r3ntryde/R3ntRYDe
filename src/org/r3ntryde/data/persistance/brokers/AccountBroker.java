package org.r3ntryde.data.persistance.brokers;


import org.r3ntryde.data.dto.AccountData;
import org.r3ntryde.data.dto.DataTransferObject;

import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

public class AccountBroker extends FileBroker {

    private static File saveFile = new File("savedAccounts.ser");

    public AccountBroker() {

        try {
            saveFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public DataTransferObject findInStorage(DataTransferObject dto) {

        AccountData dataToLoad = (AccountData) dto;
        for (DataTransferObject o : loadFile()) {
            AccountData loadedData = (AccountData) o;
            if (loadedData.getID().equals(dataToLoad.getID()) || loadedData.getAccountName().equals(dataToLoad.getAccountName())) {
                return o;
            }
        }
        return null;
    }

    public void saveIntoStorage(DataTransferObject dto) {

        deleteFromStorage(dto);
        ArrayList<DataTransferObject> allAccounts = loadFile();
        allAccounts.add(dto);
        saveFile(allAccounts);
    }

    public void deleteFromStorage(DataTransferObject dto) {

        ArrayList<DataTransferObject> allAccounts = loadFile();
        DataTransferObject accountToDelete = null;
        for (DataTransferObject o : allAccounts) {
            if (o.getID().equals(dto.getID())) {
                accountToDelete = o;
            }
        }
        allAccounts.remove(accountToDelete);
        saveFile(allAccounts);
    }

    public void resetSaveFile(DataTransferObject dto) {
        ArrayList<DataTransferObject> allAccounts = new ArrayList<>();
        allAccounts.add(dto);
        saveFile.delete();
        saveFile(allAccounts);
    }

    public ArrayList<DataTransferObject> loadFile() {

        try {
            FileInputStream fis = new FileInputStream(saveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<DataTransferObject> allAccounts = (ArrayList<DataTransferObject>) ois.readObject();
            ois.close();
            return allAccounts;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveFile(ArrayList<DataTransferObject> objectsToSave) {

        try {
            FileOutputStream fos = new FileOutputStream(saveFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(objectsToSave);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
