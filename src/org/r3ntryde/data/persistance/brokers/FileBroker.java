package org.r3ntryde.data.persistance.brokers;

import org.r3ntryde.data.dto.DataTransferObject;

import java.util.*;

public abstract class FileBroker {

    HashMap<UUID, DataTransferObject> cache = new HashMap<>();

    public DataTransferObject find(DataTransferObject dto, FileBroker broker) {

        if(cache.get(dto.getID()) != null) {
            return cache.get(dto.getID());
        }

        return broker.findInStorage(dto);
    }

    public void save(DataTransferObject dto, FileBroker broker) {
        cache.put(dto.getID(), dto);
        broker.saveIntoStorage(dto);
    }

    public void delete(DataTransferObject dto, FileBroker broker) {

        cache.remove(dto.getID());
        broker.deleteFromStorage(dto);
    }

    public abstract DataTransferObject findInStorage(DataTransferObject dto);
    public abstract void saveIntoStorage(DataTransferObject dto);
    public abstract void deleteFromStorage(DataTransferObject dto);
    public abstract ArrayList<DataTransferObject> loadFile();
    public abstract void resetSaveFile(DataTransferObject dto);
}
