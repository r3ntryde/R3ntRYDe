package org.r3ntryde.data.persistance.facade;

import org.r3ntryde.data.dto.*;
import org.r3ntryde.data.persistance.brokers.AccountBroker;
import org.r3ntryde.data.persistance.brokers.FileBroker;
import org.r3ntryde.data.persistance.brokers.VehicleBroker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

//Sköter all hantering inom persistance-delen av datalagret. Hanterar sparningar, laddningar och borttagningar av objekt.


public class PersistanceFacade extends Observable implements IPersistanceFacade {

    private HashMap<String, FileBroker> brokers = new HashMap<>();

    public PersistanceFacade() {
        brokers.put("AdminData", new AccountBroker());
        brokers.put("UserData", new AccountBroker());
        brokers.put("VehicleData", new VehicleBroker());
    }

    //Generisk metod för att spara objekt. Den hämtar filebrokern från objektet som ska sparas och får bara en broker om objektet är i rätt state.
    //Brokern sköter sen sparningen av objektet till fil.

    @Override
    public void saveObject(DataTransferObject dto) {
        FileBroker broker = brokers.get(dto.getClass().getSimpleName());
        broker.save(dto, broker);

    }

    //Metoden som säger åt brokern att ta bort objekt. När objektet är borttaget så säger den åt observern att uppdatera UI.

    @Override
    public void deleteObject(DataTransferObject dto) {
        FileBroker broker = brokers.get(dto.getClass().getSimpleName());
        broker.delete(dto, broker);
        setChanged();
        notifyObservers();
    }

    //Den generiska metoden som laddar objekt med hjälp av dess ID.

    @Override
    public DataTransferObject loadObject(DataTransferObject dto) {
        FileBroker broker = brokers.get(dto.getClass().getSimpleName());
        return broker.find(dto, broker);
    }

    @Override
    public ArrayList<DataTransferObject> loadAllObjects(DataTransferObject dto) {
        FileBroker broker = brokers.get(dto.getClass().getSimpleName());
        return broker.loadFile();
    }

    //Nollställer sparfil.

    @Override
    public void resetSave(DataTransferObject dto) {

        for(FileBroker o : brokers.values()) {
            o.resetSaveFile(dto);
        }

    }

}
