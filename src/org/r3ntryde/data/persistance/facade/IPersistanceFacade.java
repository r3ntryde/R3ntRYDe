package org.r3ntryde.data.persistance.facade;



import org.r3ntryde.data.dto.DataTransferObject;
import java.util.ArrayList;
import java.util.UUID;

public interface IPersistanceFacade {

    void saveObject(DataTransferObject dto);
    void deleteObject(DataTransferObject dto);
    DataTransferObject loadObject(DataTransferObject dto);
    ArrayList<DataTransferObject> loadAllObjects(DataTransferObject dto);
    void resetSave(DataTransferObject dto);
}
