package org.r3ntryde.domain.factory;


import org.r3ntryde.domain.classes.*;

import java.time.LocalDate;
import java.util.UUID;

public class ObjectFactory {

    public static Account getAccount(String accountType, String name, String pass, String fullName, String adress, UUID id) {

        if(accountType.equals("User") || accountType.equals("org.r3ntryde.data.dto.UserData")) {
            return new User(name, pass, fullName, adress, id);
        }
        if(accountType.equals("Admin") || accountType.equals("org.r3ntryde.data.dto.AdminData")) {
            return new Admin(name, pass, id);
        }

        return null;
    }

    public static Vehicle getVehicle(String serial, String model, String color, int year, UUID id) {
        return new Vehicle(serial, model, color, year, id);
    }

    public static Booking getBooking(LocalDate start, int duration, UUID bookingID, UUID vehicleID) {
        return new Booking(start, duration, bookingID, vehicleID);
    }

}
