package org.r3ntryde.domain.factory;

import org.r3ntryde.data.dto.AdminData;
import org.r3ntryde.data.dto.BookingData;
import org.r3ntryde.data.dto.UserData;
import org.r3ntryde.data.dto.VehicleData;

import java.time.LocalDate;
import java.util.UUID;

public class DTOFactory {

    public AdminData getAdminData(UUID accountID, String name, String pass) {
        return new AdminData(name, pass, accountID);
    }

    public UserData getUserData(UUID accountID, String accName, String pass, String fullName, String adress ) {
        return new UserData(accName, pass, fullName, adress, accountID);
    }

    public VehicleData getVehicleData(UUID vehicleID, String serial, String model, String color, int prodYear) {
        return new VehicleData(serial, model, color, prodYear, vehicleID);
    }

    public BookingData getBookingData(UUID bookingID, UUID vehicleID, LocalDate start, int duration) {
        return new BookingData(start, duration, bookingID, vehicleID);
    }
}
