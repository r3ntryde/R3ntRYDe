package org.r3ntryde.domain.facade;


import org.r3ntryde.domain.classes.Account;
import org.r3ntryde.domain.classes.Booking;
import org.r3ntryde.domain.classes.Vehicle;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public interface IDomainFacade {

    Account createAccount(String name, String pass, String fullName, String adress, String accType);
    Account loadAccount(UUID id);
    Account login(String inputName, String inputPass);

    Vehicle createVehicle(String vehicleSerial, String vehicleModel, String vehicleColor, int year);
    Vehicle loadVehicle(UUID id);
    HashMap<UUID, Vehicle> loadAllVehicles();
    HashMap<UUID, Vehicle> loadFreeVehicles(LocalDate start, int duration);
    void deleteVehicle(UUID id);

    Booking createBooking(LocalDate start, int duration, Account user, UUID vehicleID);
    HashMap<UUID, Booking> loadAllBookings(UUID userID);
    void deleteBooking(UUID bookingID, UUID userID);

    void resetSave();



}
