package org.r3ntryde.domain.facade;

import org.r3ntryde.data.dto.*;
import org.r3ntryde.data.facade.IDataFacade;
import org.r3ntryde.domain.classes.*;
import org.r3ntryde.domain.factory.DTOFactory;
import org.r3ntryde.domain.factory.ObjectFactory;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Observable;
import java.util.UUID;

//Kontrollerar alla funktioner i domainlagret.

public class DomainFacade extends Observable implements IDomainFacade {

    private IDataFacade data;
    private ObjectFactory domainObjectFactory;
    private DTOFactory dtoFactory;

    public DomainFacade(IDataFacade data) {

        this.data = data;
        domainObjectFactory = new ObjectFactory();
        dtoFactory = new DTOFactory();
    }

    //Alla metoder som rör kontohantering inom domainlagret och kommunikation av konton till datalagret.

    @Override
    public Account createAccount(String name, String pass, String fullName, String adress, String accType) {

        if(data.loadObject(dtoFactory.getUserData(null, name, null, null, null)) == null) {

            UUID id = UUID.randomUUID();
            Account newAccount = domainObjectFactory.getAccount(accType, name, pass, fullName, adress, id);
            if (accType.equals("User")) {
                data.saveObject(dtoFactory.getUserData(id, name, pass, fullName, adress));
            }
            if (accType.equals("Admin")) {
                data.saveObject(dtoFactory.getAdminData(id, name, pass));
            }

            return newAccount;
        }
        System.out.println("Account name is already taken!");
        return null;
    }

    @Override
    public Account loadAccount(UUID id) {

        UserData account = (UserData) data.loadObject(dtoFactory.getUserData(id, null, null, null, null));
        return domainObjectFactory.getAccount(account.getClass().getName(), account.getAccountName(), account.getPass(), account.getFullName(), account.getAdress(), account.getID());
    }



    //Alla metoder som rör fordonshantering inom domainlagret och kommunikation av fordon till datalagret.

    @Override
    public Vehicle createVehicle(String serial, String model, String color, int year) {

        if(data.loadObject(dtoFactory.getVehicleData(null, serial, null, null, 0)) == null) {

            UUID id = UUID.randomUUID();
            Vehicle newVehicle = domainObjectFactory.getVehicle(serial, model, color, year, id);
            data.saveObject(dtoFactory.getVehicleData(id, serial, model, color, year));
            return newVehicle;
        }
        return null;
    }

    @Override
    public Vehicle loadVehicle(UUID id) {
        VehicleData vehicle = (VehicleData) data.loadObject(dtoFactory.getVehicleData(id, null, null, null, 0));

        return domainObjectFactory.getVehicle(vehicle.getSerial(), vehicle.getModel(), vehicle.getColor(), vehicle.getProdYear(), vehicle.getID());
    }

    @Override
    public HashMap<UUID, Vehicle> loadAllVehicles() {

       HashMap<UUID, Vehicle> allVehicles = new HashMap<>();

        for(DataTransferObject o : data.loadAllObjects(dtoFactory.getVehicleData(null, null, null, null, 0))) {
            VehicleData vehicleData = (VehicleData) o;
            allVehicles.put(o.getID(), domainObjectFactory.getVehicle(vehicleData.getSerial(), vehicleData.getModel(), vehicleData.getColor(), vehicleData.getProdYear(), vehicleData.getID()));
        }

        return allVehicles;
    }

    @Override
    public HashMap<UUID, Vehicle> loadFreeVehicles(LocalDate startIns, int duration) {

        HashMap<UUID, Vehicle> freeVehicles = new HashMap<>();
        LocalDate endIns = startIns.plusDays(duration);

        for(DataTransferObject o : data.loadAllObjects(dtoFactory.getVehicleData(null, null, null, null, 0))) {
            boolean vehicleFree = true;
            VehicleData vehicleData = (VehicleData) o;
            for(BookingData i : vehicleData.getBookings().values()) {
                LocalDate startLoad = i.getStart();
                LocalDate endLoad = i.getStart().plusDays(i.getDuration());

                if((startIns.isBefore(startLoad) && endIns.isAfter(endLoad))
                        || (startIns.isBefore(startLoad) && endIns.isAfter(startLoad))
                        || (startIns.isBefore(endLoad) && endIns.isAfter(endLoad))
                        || (startIns.isAfter(startLoad) && endIns.isBefore(endLoad))
                        || (startIns.isEqual(startLoad) || startIns.isEqual(endLoad) || endIns.isEqual(startLoad) || endIns.isEqual(endLoad))) {

                    vehicleFree = false;

                }
            }
            if(vehicleFree) {
                freeVehicles.put(vehicleData.getID(), domainObjectFactory.getVehicle(vehicleData.getSerial(), vehicleData.getModel(), vehicleData.getColor(), vehicleData.getProdYear(), o.getID()));
            }
        }
        return freeVehicles;
    }

    @Override
    public void deleteVehicle(UUID vehicleID) {
        data.deleteObject(dtoFactory.getVehicleData(vehicleID, null, null, null, 0));
    }




    //Alla metoder som rör bokningsshantering inom domainlagret och kommunikation av bokningar till datalagret.

    @Override
    public Booking createBooking(LocalDate start, int duration, Account user, UUID vehicleID) {

        UUID bookingID = UUID.randomUUID();
        Booking newBooking = domainObjectFactory.getBooking(start, duration, bookingID, vehicleID);
        VehicleData vehicleData = (VehicleData) data.loadObject(dtoFactory.getVehicleData(vehicleID, null, null, null, 0));
        UserData userData = (UserData) data.loadObject(dtoFactory.getUserData(user.getID(), null, null, null, null));
        user.addBooking(newBooking);
        BookingData bookingData = dtoFactory.getBookingData(bookingID, vehicleID, start, duration);
        vehicleData.addBooking(bookingData);
        userData.addBooking(bookingData);
        data.saveObject(vehicleData);
        data.saveObject(userData);
        return newBooking;
    }

    @Override
    public HashMap<UUID, Booking> loadAllBookings(UUID userID) {

        HashMap<UUID, Booking> allBookings = new HashMap<>();
        UserData userData = (UserData) data.loadObject(dtoFactory.getUserData(userID, null, null, null, null));
        for(BookingData o : userData.getBookings().values()) {
            allBookings.put(o.getID(), domainObjectFactory.getBooking(o.getStart(), o.getDuration(), o.getID(), o.getVehicleID()));
        }

        return allBookings;
    }

    @Override
    public void deleteBooking(UUID bookingID, UUID userID) {
        UserData userData = (UserData) data.loadObject(dtoFactory.getUserData(userID, null, null, null, null));
        VehicleData vehicleData = (VehicleData) data.loadObject(dtoFactory.getVehicleData(userData.getBookings().get(bookingID).getVehicleID(), null, null, null, 0));
        vehicleData.removeBooking(bookingID);
        userData.removeBooking(bookingID);
        data.saveObject(userData);
        data.saveObject(vehicleData);
        setChanged();
        notifyObservers();

    }

    @Override
    public Account login(String inputName, String inputPass) {

        DataTransferObject dto =  data.loadObject(dtoFactory.getUserData(null, inputName, null, null, null));
        if(dto != null) {
            if(dto.getClass().getSimpleName().equals("UserData")) {
                UserData loginAccount = (UserData) dto;
                if (loginAccount.getPass().equals(inputPass)) {
                    return domainObjectFactory.getAccount(loginAccount.getClass().getName(), loginAccount.getAccountName(), loginAccount.getPass(), loginAccount.getFullName(), loginAccount.getAdress(), loginAccount.getID());
                }
            }
            if(dto.getClass().getSimpleName().equals("AdminData")) {
                AdminData loginAccount = (AdminData) dto;
                if (loginAccount.getPass().equals(inputPass)) {
                    return domainObjectFactory.getAccount (loginAccount.getClass().getName(), loginAccount.getAccountName(), loginAccount.getPass(), null, null, loginAccount.getID());
                }
            }
        }
        return null;
    }

    //En metod som nollställer sparfilen och skapar ett adminkonto.

    @Override
    public void resetSave() {

        data.resetSave(dtoFactory.getAdminData(UUID.randomUUID(), "admin", "adminpass"));
    }

}
