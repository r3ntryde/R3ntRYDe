package org.r3ntryde.domain.classes;


import java.util.UUID;

public class Admin extends Account {

    public Admin(String name, String pass, UUID id) {

        super(name, pass, id);
    }
}
