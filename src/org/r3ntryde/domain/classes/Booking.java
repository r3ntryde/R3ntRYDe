package org.r3ntryde.domain.classes;

import java.time.LocalDate;
import java.util.UUID;


public class Booking extends DomainObject {

    private LocalDate start;
    private int duration;
    private UUID vehicleID;

    public Booking(LocalDate start, int duration, UUID id, UUID vehicleID) {
        super(id);
        this.start = start;
        this.duration = duration;
        this.vehicleID = vehicleID;

    }
    public LocalDate getStartDate (){ return start; }
    public int getDuration(){ return duration;}

    public UUID getVehicleID() {
        return vehicleID;
    }
}
