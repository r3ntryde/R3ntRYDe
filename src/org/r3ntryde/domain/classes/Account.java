package org.r3ntryde.domain.classes;


import java.util.UUID;

public abstract class Account extends DomainObject {

    private String accountName;
    private String password;

    public Account(String name, String pass, UUID id) {
        super(id);
        accountName = name;
        password = pass;
    }

    public String getName() {
        return accountName;
    }

    public String getPass() {
        return password;
    }

    public void addBooking(Booking booking){};
    public void deleteBooking(UUID bookingID){};


}
