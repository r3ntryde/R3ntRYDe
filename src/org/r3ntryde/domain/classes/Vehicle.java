package org.r3ntryde.domain.classes;

import java.util.UUID;

public class Vehicle extends DomainObject {

    private String serial;
    private String model;
    private String color;
    private int prodYear;

    public Vehicle(String serial, String model, String color, int prodYear, UUID id){
        super(id);
        this.serial = serial;
        this.model = model;
        this.color = color;
        this.prodYear = prodYear;
    }

    public String getSerial() {return serial;}
    public String getModel() {return model;}
    public String getColor() {return color;}
    public int getProdYear() {return prodYear;}

}
