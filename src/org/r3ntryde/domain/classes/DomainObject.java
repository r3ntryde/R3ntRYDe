package org.r3ntryde.domain.classes;

import java.util.UUID;

public abstract class DomainObject {

    private UUID id;

    public DomainObject(UUID id) {
        this.id = id;
    }

    public UUID getID() {
        return id;
    }
}
