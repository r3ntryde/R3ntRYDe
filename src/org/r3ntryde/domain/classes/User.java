package org.r3ntryde.domain.classes;


import java.util.ArrayList;
import java.util.UUID;

public class User extends Account {

    private ArrayList<Booking> bookings = new ArrayList<Booking>();

    private String fullName;
    private String adress;

    public User(String accountName, String pass, String fullName, String adress, UUID id){
        super(accountName, pass, id);
        this.fullName = fullName;
        this.adress = adress;
    }

    public void addBooking(Booking newBooking) {

        bookings.add(newBooking);
    }

    public void deleteBooking(UUID bookingID) {

        for(Booking i : bookings) {
            if(i.getID() == bookingID ) {
                bookings.remove(i);
            }
        }
    }

    public String getFullName() { return fullName; }
    public String getAdress() { return adress; }



}
