package org.r3ntryde.ui.controller;
import org.r3ntryde.domain.classes.Account;
import org.r3ntryde.domain.classes.Booking;
import org.r3ntryde.domain.classes.User;
import org.r3ntryde.domain.classes.Vehicle;
import org.r3ntryde.domain.facade.IDomainFacade;
import org.r3ntryde.ui.views.*;
import org.r3ntryde.ui.frame.Frame;
import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.util.*;

public class ModelViewController implements Observer {

    private JFrame frame;
    private IDomainFacade domain;
    private JPanel currentView;
    private JPanel bottomMenu;
    private Account accountModel;
    private HashMap<UUID, Vehicle> vehicleModel = new HashMap<>();
    private HashMap<UUID, Booking> bookingModel = new HashMap<>();

    public ModelViewController(IDomainFacade domainFacade) {

        domain = domainFacade;
        frame = new Frame();
        currentView = new LoginScreen(this);
        frame.add(currentView);
        frame.revalidate();
        frame.repaint();
    }

    public void changeView(JPanel newView) {

        frame.remove(currentView);
        frame.add(newView);
        currentView = newView;
        frame.revalidate();
        frame.repaint();
    }

    public boolean validateLogin(String userName, String password) {

        accountModel = domain.login(userName, password);

        if(accountModel != null) {
            if(accountModel.getClass().getSimpleName().equals("User")) {
                bottomMenu = new BottomMenu(this);
                frame.add(bottomMenu, BorderLayout.SOUTH);
                loadAllBookings();
                changeView( new StartPage(this));
            } else {
                changeView(new AdminCarList(this));
            }

            return true;
        }
        return false;
    }

    public void createAccount(String accountName, String firstPass, String secondPass, String fullName, String adress) {
        if(firstPass.equals(secondPass)) {
            domain.createAccount(accountName, firstPass, fullName, adress, "User");
            validateLogin(accountName, firstPass);
        } else {
            JOptionPane.showMessageDialog(null, "The passwords did not match!");
        }
    }

    public User getAccountModel() {
        return (User) accountModel;
    }


    public HashMap<UUID, Vehicle> loadAllVehicles() {

        if(vehicleModel.isEmpty()) {
            vehicleModel = domain.loadAllVehicles();
        }

        return vehicleModel;
    }

    public HashMap<UUID, Vehicle> loadFreeVehicles(LocalDate start, int duration) {
        vehicleModel.clear();
        vehicleModel = domain.loadFreeVehicles(start, duration);

        return vehicleModel;
    }

    public Vehicle loadVehicle(UUID id) {
        if(!vehicleModel.containsKey(id)) {
            Vehicle loadedVehicle = domain.loadVehicle(id);
            vehicleModel.put(loadedVehicle.getID(), loadedVehicle);
        }
        return vehicleModel.get(id);

    }

    public void createVehicle(String serial, String model, String color, int prodYear) {
        Vehicle newVehicle = domain.createVehicle(serial, model, color, prodYear);
        if(newVehicle != null) {
            vehicleModel.put(newVehicle.getID(), newVehicle);
        } else {
            JOptionPane.showMessageDialog(null, "Vehicle serial already exists in database!");
        }
    }

    public void deleteVehicle(UUID id) {

        vehicleModel.remove(id);
        domain.deleteVehicle(id);
    }

    public void createBooking(LocalDate start, int duration, UUID vehicleID){
        Booking newBooking = domain.createBooking(start, duration, accountModel, vehicleID );
        bookingModel.put(newBooking.getID(), newBooking);
    }

    public void deleteBooking(UUID id) {

        bookingModel.remove(id);
        domain.deleteBooking(id, accountModel.getID());
    }


    public HashMap<UUID, Booking> loadAllBookings() {

        if(bookingModel.isEmpty()) {
          bookingModel = domain.loadAllBookings(accountModel.getID());
        }

        return bookingModel;
    }


    @Override
    public void update(Observable o, Object arg) {
        frame.remove(currentView);
        System.out.println("TEST");
        if(currentView.getClass().getSimpleName().equals("AdminCarList")) {
            currentView = new AdminCarList(this);
        }
        if(currentView.getClass().getSimpleName().equals("BookingList")) {
            currentView = new BookingList(this);
        }
        frame.add(currentView);
        frame.revalidate();
        frame.repaint();
    }
}
