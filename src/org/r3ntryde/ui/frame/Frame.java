package org.r3ntryde.ui.frame;

import javax.swing.*;


public class Frame extends JFrame {

    public Frame() {

        super("Rent & RYDe");
        setSize(480, 820 );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);
    }
}
