package org.r3ntryde.ui.views;

import org.r3ntryde.ui.controller.ModelViewController;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class SignUpScreen extends JPanel implements ActionListener {

    private JTextField textFieldName;
    private JPasswordField passFieldOne;
    private JPasswordField passFieldTwo;
    private JTextField textFieldAdress;
    private JTextField textFieldUsername;
    private JFrame frame;

    private JPanel panelSignUp;
    private JPanel panelLogo;
    private JPanel panelButton;
    private ModelViewController controller;

    public SignUpScreen(ModelViewController controller) {

        panelSignUp = new JPanel();
        panelLogo = new JPanel();
        panelButton = new JPanel();

        this.controller = controller;

        Image img = null;
        try {
            img = ImageIO.read(getClass().getResource("images/tent.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JLabel picLabel = new JLabel(new ImageIcon(img));
        panelLogo.add(picLabel);


        textFieldAdress = new JTextField();
        textFieldName = new JTextField();
        textFieldUsername = new JTextField();
        passFieldOne = new JPasswordField();
        passFieldTwo = new JPasswordField();

        textFieldAdress.setPreferredSize(new Dimension(380, 40));
        textFieldName.setPreferredSize(new Dimension(380, 40));
        textFieldUsername.setPreferredSize(new Dimension(380, 40));
        passFieldOne.setPreferredSize(new Dimension(380, 40));
        passFieldTwo.setPreferredSize(new Dimension(380, 40));

        JButton btnSignUp = new JButton("Sign up");
        JButton btnBack = new JButton("Back");

        JLabel labelName = new JLabel("Full name");
        JLabel labelUsername = new JLabel("Username");
        JLabel labelAdress = new JLabel("Adress");
        JLabel labelPassFieldOne = new JLabel("Password");
        JLabel labelPassFieldTwo = new JLabel("Enter password again");

        Font myFont = new Font("ARIAL BLACK", Font.BOLD, 18);
        labelName.setFont(myFont);
        labelUsername.setFont(myFont);
        labelAdress.setFont(myFont);
        labelPassFieldOne.setFont(myFont);
        labelPassFieldTwo.setFont(myFont);
        btnBack.setFont(myFont);
        btnSignUp.setFont(myFont);

        panelSignUp.add(labelName);
        panelSignUp.add(textFieldName);
        panelSignUp.add(labelUsername);
        panelSignUp.add(textFieldUsername);
        panelSignUp.add(labelPassFieldOne);
        panelSignUp.add(passFieldOne);
        panelSignUp.add(labelPassFieldTwo);
        panelSignUp.add(passFieldTwo);
        panelSignUp.add(labelAdress);
        panelSignUp.add(textFieldAdress);

        panelButton.add(btnBack);
        panelButton.add(btnSignUp);
        btnBack.addActionListener(this);
        btnSignUp.addActionListener(this);

        btnSignUp.setPreferredSize(new Dimension(120, 50));
        btnBack.setPreferredSize(new Dimension(120, 50));

        Border empty;
        empty = BorderFactory.createEmptyBorder(20, 10, 10, 10);
        panelLogo.setBorder(empty);
        panelSignUp.setBorder(empty);

        Border button;
        button = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        panelButton.setBorder(button);

        panelSignUp.setBackground(Color.white);
        panelLogo.setBackground(Color.white);
        panelButton.setBackground(Color.white);

        setLayout(new BorderLayout());
        add(panelLogo, BorderLayout.NORTH);
        add(panelSignUp, BorderLayout.CENTER);
        add(panelButton, BorderLayout.SOUTH);

    }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("Back")) {
                controller.changeView(new LoginScreen(controller));
            }
            if (e.getActionCommand().equals("Sign up")) {
                String name = textFieldName.getText();
                String userName = textFieldUsername.getText();
                String adress = textFieldAdress.getText();
                String passwordOne = passFieldOne.getText();
                String passwordTwo = passFieldTwo.getText();

              controller.createAccount(userName, passwordOne, passwordTwo, name, adress);

            }

        }


}
