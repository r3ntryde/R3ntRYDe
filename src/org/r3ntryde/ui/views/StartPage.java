package org.r3ntryde.ui.views;

import org.r3ntryde.ui.controller.ModelViewController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.io.IOException;


/**
 * Created by rasmuu on 2017-05-04.
 */
public class StartPage extends JPanel {
    public StartPage(ModelViewController controller){



        Image img = null;
        try {
            img = ImageIO.read(getClass().getResource("images/tent.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JLabel picLabel = new JLabel(new ImageIcon(img));
        //add(picLabel);

            JLabel welcome = new JLabel("Welcome "+ "user" +"!", SwingConstants.CENTER);
            welcome.setBounds(10, 180, 470, 235);
             Font headFont = new Font("ARIAL BLACK", Font.BOLD, 20);
             welcome.setFont(headFont);


            JLabel info = new JLabel("<html>Want to rent a car?<br> Then this is the app for you!<br>" +
                    "The menu at the bottom allows you to navigate<br> easily through the application.<br> The Book a car button allows you to enter booking details<br> to book one of our fantastic cars. <br>" +
                    "If you want to managae your bookings,<br> the My bookings button provides information for that.<br>" +
                    "To manage personal information, enter<br> the profile view by using the My profile button.<br>" +
                    "We hope you find this application useful <br>and that you will find a perfect car to rent! <br>If you would like"+
                    " to access this information again, <br>use the Home button</html>" );
            Font myFont = new Font("ARIAL BLACK", Font.BOLD, 12);
            info.setFont(myFont);


            setLayout(new BorderLayout());
            add(picLabel, BorderLayout.NORTH);
            setBackground(Color.WHITE);
            add(welcome);
            add(info);














        }
    }

