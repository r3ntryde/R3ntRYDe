package org.r3ntryde.ui.views;

import org.r3ntryde.ui.controller.ModelViewController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Created by rasmu on 2017-05-24.
 */
public class ProfileScreen extends JPanel {
    private String usr;
    private String pw;
    private String name;
    private String adress;
    private JFrame frame;

    private JLabel lPic;
    private JLabel lName;
    private JLabel lUser;
    private JLabel lAdress;

    private ModelViewController controller;

    public ProfileScreen(ModelViewController controller) {
        this.controller = controller;
        setBackground(Color.WHITE);

        Font myFont = new Font("ARIAL BLACK", Font.BOLD, 16);

        Image img = null;
        try {
            img = ImageIO.read(getClass().getResource("images/profile.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JLabel picLabel = new JLabel(new ImageIcon(img));


        JLabel lName = new JLabel("Name: " + controller.getAccountModel().getFullName());
        JLabel lUser = new JLabel("Username: " + controller.getAccountModel().getName());
        JLabel lAdress = new JLabel("Adress: " + controller.getAccountModel().getAdress());
        JLabel dummy = new JLabel("");


        lName.setBounds(30, 275, 360, 40);
        lUser.setBounds(30, 310, 360, 40);
        lAdress.setBounds(30, 345, 360, 40);

        lName.setFont(myFont);
        lUser.setFont(myFont);
        lAdress.setFont(myFont);

        setLayout(new BorderLayout());
        add(picLabel, BorderLayout.NORTH);
        add(lName, BorderLayout.CENTER);
        add(lUser, BorderLayout.CENTER);
        add(lAdress, BorderLayout.CENTER);
        add(dummy, BorderLayout.CENTER);
    }

}
