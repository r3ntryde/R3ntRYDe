package org.r3ntryde.ui.views;

import org.r3ntryde.ui.controller.ModelViewController;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.DateFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by rasmu on 2017-05-08.
 */
public class BookCar extends JPanel {
    private String sd;
    private String nrDaysStr;
    private ModelViewController controller;

    public BookCar(ModelViewController controller){

        this.controller = controller;
        JLabel head = new JLabel("Boka bil", SwingConstants.CENTER);
        head.setFont(new Font("Calibri", Font.BOLD, 31));
        JLabel text = new JLabel("Vänligen välj starttid för hyra av bilen:");
        JLabel nrDaysText = new JLabel("Mata in antal dagar du vill låna den:");
        setLayout(new BorderLayout());

        JPanel field1 = new JPanel();
        JPanel field2 = new JPanel();
        JButton submit = new JButton("Search available car");


        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        DateFormatter df = new DateFormatter(format);
        JFormattedTextField dateField = new JFormattedTextField(df);
        dateField.setPreferredSize(new Dimension(450, 40));
        dateField.setValue(new Date());

        JTextField nrDays = new JTextField();
        nrDays.setPreferredSize(new Dimension(450, 40));



        sd = "";
        nrDaysStr = "0";
        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
             if  (e.getActionCommand().equals("Search available car"))
                sd = dateField.getText();
                LocalDate ld = LocalDate.parse(sd);
                nrDaysStr = nrDays.getText();
                int nrDaysInt = Integer.parseInt(nrDaysStr);
                controller.changeView(new ShowAvailableCars(controller, ld, nrDaysInt));

            }
        });
        Border empty;
        empty = BorderFactory.createEmptyBorder(100, 10 , 10, 10);

        field2.setBorder(empty);
        field2.setLayout(new BorderLayout());
        field1.add(text);
        field1.add(dateField);
        field1.add(nrDaysText);

        field1.setBackground(Color.WHITE);
        field2.setBackground(Color.WHITE);

        field1.add(nrDays);
        field1.add(submit);
        field2.add(head, BorderLayout.NORTH);


        add(field2, BorderLayout.NORTH);

        add(field1, BorderLayout.CENTER);

    }

}
