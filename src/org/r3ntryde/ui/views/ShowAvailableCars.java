package org.r3ntryde.ui.views;

import org.r3ntryde.domain.classes.Vehicle;
import org.r3ntryde.ui.controller.ModelViewController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class ShowAvailableCars extends JPanel implements ActionListener{
    private ModelViewController controller;
    private LocalDate startDate;
    private int duration;

    public ShowAvailableCars(ModelViewController controller, LocalDate startDate, int duration){
       this.controller = controller;
       this.startDate = startDate;
       this.duration = duration;
       
       setBackground(Color.white);
       setLayout(new FlowLayout());
       setVisible(true);



        HashMap<UUID, Vehicle> availableVehicles = controller.loadFreeVehicles(startDate, duration);

        Font myFont = new Font("ARIAL BLACK", Font.BOLD, 18);



        for(Vehicle v : availableVehicles.values()){
            JPanel availableVehiclesPanel = new JPanel();
            availableVehiclesPanel.setLayout(new GridLayout(2, 3));
            availableVehiclesPanel.setPreferredSize(new Dimension(460, 70));
            availableVehiclesPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10 ));
            availableVehiclesPanel.setBorder(BorderFactory.createLineBorder(Color.black));

            JTextArea serial = new JTextArea();
            serial.setText(v.getSerial());
            serial.setFont(myFont);
            availableVehiclesPanel.add(serial);

            JTextArea model = new JTextArea();
            model.setText(v.getModel());
            model.setFont(myFont);
            availableVehiclesPanel.add(model);

            JTextArea color = new JTextArea();
            color.setText(v.getColor());
            color.setFont(myFont);
            availableVehiclesPanel.add(color);

            JTextArea prodYear = new JTextArea();
            prodYear.setText(String.valueOf(v.getProdYear()));
            prodYear.setFont(myFont);
            availableVehiclesPanel.add(prodYear);



            JButton btnBook = new JButton("Book");
            btnBook.addActionListener(this);
            btnBook.putClientProperty("id", v.getID());
            availableVehiclesPanel.add(btnBook);
            add(availableVehiclesPanel);
        }

        JButton btnBack = new JButton("Back");
        btnBack.addActionListener(this);
        add(btnBack);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("Book")) {
            JButton source = (JButton)e.getSource();
            controller.createBooking(startDate, duration, (UUID)source.getClientProperty("id"));
            controller.changeView(new BookingList(controller));

        } if (e.getActionCommand().equals("Back")) {
            controller.changeView(new StartPage(controller));
        }

    }

}
