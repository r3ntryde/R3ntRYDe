package org.r3ntryde.ui.views;

import org.r3ntryde.domain.classes.Booking;
import org.r3ntryde.ui.controller.ModelViewController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.UUID;

public class BookingList extends JPanel implements ActionListener {
    private ModelViewController controller;
    private JPanel panelBook;
    public BookingList(ModelViewController controller){
        this.controller = controller;

        setBackground(Color.white);

        HashMap<UUID, Booking> allBookings = controller.loadAllBookings();

        for(Booking o : allBookings.values()) {
            JPanel bookingPanel = new JPanel();
            bookingPanel.setLayout(new GridLayout(2, 3));
            bookingPanel.setPreferredSize(new Dimension(460, 70));
            bookingPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10 ));
            bookingPanel.setBorder(BorderFactory.createLineBorder(Color.black));
            JTextArea startDate = new JTextArea();
            LocalDate date = o.getStartDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yy");
            startDate.setText("Start date: " + date.format(formatter));
            startDate.setFont(new Font("ARIAL BLACK", Font.BOLD, 18));
            bookingPanel.add(startDate);
            JTextArea endDate = new JTextArea();
            date = o.getStartDate().plusDays(o.getDuration());
            endDate.setText("End date: " + date.format(formatter));
            endDate.setFont(new Font("ARIAL BLACK", Font.BOLD, 18));
            bookingPanel.add(endDate);
            JTextArea vehicleSerial = new JTextArea();
            vehicleSerial.setText(controller.loadVehicle(o.getVehicleID()).getSerial());
            vehicleSerial.setFont(new Font("ARIAL BLACK", Font.BOLD, 18));
            bookingPanel.add(vehicleSerial);
            JButton delete = new JButton("Remove booking");
            delete.addActionListener(this);
            delete.putClientProperty("id", o.getID());
            bookingPanel.add(delete);
            add(bookingPanel);
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getActionCommand().equals("Remove booking")) {
            JButton source = (JButton)e.getSource();
            controller.deleteBooking((UUID)source.getClientProperty("id"));
        }

    }
}



