
package org.r3ntryde.ui.views;

import org.r3ntryde.ui.controller.ModelViewController;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;


public class BottomMenu extends JPanel {
    private JButton bookCar, bookings, profile, home;
    public BottomMenu(ModelViewController controller){

        ActionListener actionListener = e -> {
            if (e.getActionCommand().equals("Boka bil")) {
                controller.changeView(new BookCar(controller));
            } else if (e.getActionCommand().equals("Mina bokningar")) {
                controller.changeView(new BookingList(controller));
            }else if (e.getActionCommand().equals("Min sida")){
                controller.changeView(new ProfileScreen(controller));
            }else if (e.getActionCommand().equals("Hjälp")){
              controller.changeView(new StartPage(controller));
            }
        };

        JPanel buttons = new JPanel();
        buttons.setBackground(Color.WHITE);
        bookCar = new JButton();
        bookings = new JButton();
        profile = new JButton();
        home = new JButton();

        bookCar.setActionCommand("Boka bil");
        bookings.setActionCommand("Mina bokningar");
        profile.setActionCommand("Min sida");
        home.setActionCommand("Hjälp");


        try {
            Image img = ImageIO.read(getClass().getResource("images/bookcar.png"));
            bookCar.setIcon(new ImageIcon(img));
            Image img2 = ImageIO.read(getClass().getResource("images/bookings.png"));
            bookings.setIcon(new ImageIcon(img2));
            Image img3 = ImageIO.read(getClass().getResource("images/account.png"));
            profile.setIcon(new ImageIcon(img3));
            Image img4 = ImageIO.read(getClass().getResource("images/Homebtn.png"));
            home.setIcon(new ImageIcon(img4));
        } catch (Exception ex) {
            System.out.println(ex);

        }


        bookCar.addActionListener(actionListener);
        bookings.addActionListener(actionListener);
        profile.addActionListener(actionListener);
        home.addActionListener(actionListener);



        bookCar.setMargin(new Insets(0, 0, 0, 0));
        bookCar.setFocusPainted(false);
        bookCar.setBorder(new EmptyBorder(0, 0, 0, 0));

        bookings.setMargin(new Insets(0, 0, 0, 0));
        bookings.setFocusPainted(false);
        bookings.setBorder(new EmptyBorder(0, 0, 0, 0));

        profile.setMargin(new Insets(0, 0, 0, 0));
        profile.setFocusPainted(false);
        profile.setBorder(new EmptyBorder(0, 0, 0, 0));

        home.setMargin(new Insets(0, 0, 0, 0));
        home.setFocusPainted(false);
        home.setBorder(new EmptyBorder(0, 0, 0, 0));

        buttons.add(home);
        buttons.add(bookCar);
        buttons.add(bookings);
        buttons.add(profile);

        add(buttons);

    }




}

