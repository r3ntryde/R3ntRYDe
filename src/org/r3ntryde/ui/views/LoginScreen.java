package org.r3ntryde.ui.views;

import org.r3ntryde.ui.controller.ModelViewController;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class LoginScreen extends JPanel implements ActionListener {

    private JTextField textFieldUsername;
    private JPasswordField passField;
    private JPanel panelLogin;
    private JPanel panelLogo;
    private ModelViewController controller;
    private StartPage startPage;

    public LoginScreen(ModelViewController controller){

        this.controller = controller;
        // Skapar JPanels, en för loggan och en för login funktioner
        panelLogin = new JPanel();
        panelLogo = new JPanel();

        // Skapar mina textField, rutorna där man skriver in username osv.
        // Placeholder skapar en ny placeholder, "spöktext" i rutan
        // setPrefferedSize ändrar storleken på textrutan
        textFieldUsername = new JTextField();
        passField = new JPasswordField();
        Placeholder placeholderTextUsername = new Placeholder(textFieldUsername, " Username");
        Placeholder placeholderPassword = new Placeholder(passField, "Password");
        textFieldUsername.setPreferredSize(new Dimension(360, 40));
        passField.setPreferredSize(new Dimension(360, 40));

        JButton btnLogin = new JButton("Login");
        JButton btnSignUp = new JButton("New user");
        btnLogin.addActionListener(this);
        btnSignUp.addActionListener(this);


        Font myFont = new Font("ARIAL BLACK", Font.BOLD, 16);
        btnLogin.setFont(myFont);

        Image img = null;
        try {
            img = ImageIO.read(getClass().getResource("images/tent.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JLabel picLabel = new JLabel(new ImageIcon(img));

        panelLogo.add(picLabel);

        panelLogin.add(textFieldUsername);
        panelLogin.add(passField);
        panelLogin.add(btnLogin);
        panelLogin.add(btnSignUp, BorderLayout.SOUTH);
        btnSignUp.setFont(myFont);

        Border empty = BorderFactory.createEmptyBorder(40, 10 , 10, 10);
        panelLogin.setBorder(empty);
        panelLogo.setBorder(empty);



        panelLogin.setBackground(Color.white);
        panelLogo.setBackground(Color.white);

        setLayout(new BorderLayout());
        add(panelLogo, BorderLayout.NORTH);
        add(panelLogin, BorderLayout.CENTER);


    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("New user")){
            controller.changeView(new SignUpScreen(controller));
        }
        if(e.getActionCommand().equals("Login")) {
            String userName = textFieldUsername.getText();
            String password = passField.getText();

            if(!controller.validateLogin(userName, password)){
                    JOptionPane.showMessageDialog(null, "Wrong password or username");
            }



        }
    }
}



