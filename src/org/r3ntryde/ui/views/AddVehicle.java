package org.r3ntryde.ui.views;


import org.r3ntryde.ui.controller.ModelViewController;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddVehicle extends JPanel implements ActionListener {

    private ModelViewController controller;
   private JTextField regNrTextField, modelTextField, yearTextField, colorTextField;

    public AddVehicle(ModelViewController controller) {

        this.controller = controller;
        setBackground(Color.WHITE);
        setSize(480,820);
        setLayout(null);
        setVisible(true);
        init();

    }

    public void init() {

        Font myFont = new Font("ARIAL BLACK", Font.BOLD, 13);
        JLabel headline = new JLabel("Add a car to the system");
        headline.setBounds(30, 50, 200, 30);
        headline.setFont(myFont);

        JLabel forT1 = new JLabel("Registration number");
        forT1.setBounds(30, 80, 360, 40);
        regNrTextField =new JTextField();
        regNrTextField.setBounds(30,115, 360,40);
        JLabel forT2 = new JLabel("AccountModel");
        forT2.setBounds(30, 145, 360, 40);
        modelTextField =new JTextField();
        modelTextField.setBounds(30,180, 360,40);
        JLabel forT3 = new JLabel("Year");
        forT3.setBounds(30, 210, 360, 40);
        yearTextField =new JTextField();
        yearTextField.setBounds(30,245, 360,40);
        JLabel forT4 = new JLabel("Color");
        forT4.setBounds(30, 275, 360, 40);
        colorTextField =new JTextField();
        colorTextField.setBounds(30,310, 360,40);

        JButton addVehicle = new JButton("Add");
        addVehicle.setBounds(50,370, 150, 30);

        JButton btnBack = new JButton("Back");
        btnBack.setBounds(220,370, 150, 30);
        add(btnBack);
        btnBack.addActionListener(this);
        addVehicle.addActionListener(this);
        
        add(addVehicle);
        add(headline);
        add(forT1);
        add(forT2);
        add(forT3);
        add(forT4);
        add(regNrTextField);
        add(modelTextField);
        add(yearTextField);
        add(colorTextField);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Add")) {
            String model = modelTextField.getText();
            String color = colorTextField.getText();
            String regNr = regNrTextField.getText();
            int year = Integer.parseInt(yearTextField.getText());
            JOptionPane.showMessageDialog(null, "You have added a vehicle!");
            controller.createVehicle(regNr, model, color, year);
            controller.changeView(new AdminCarList(controller));

        } if (e.getActionCommand().equals("Back")) {
            controller.changeView(new AdminCarList(controller));
        }



    }
}
