package org.r3ntryde.ui.views;

import org.r3ntryde.domain.classes.Vehicle;
import org.r3ntryde.ui.controller.ModelViewController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class AdminCarList extends JPanel implements ActionListener {

    private ModelViewController controller;

    public AdminCarList(ModelViewController controller) {

        this.controller = controller;
        setBackground(Color.white);
        setLayout(new FlowLayout());
        setVisible(true);


        HashMap<UUID, Vehicle> allVehicles = controller.loadAllVehicles();

        for(Vehicle o : allVehicles.values()) {
            JPanel vehiclePanel = new JPanel();
            vehiclePanel.setLayout(new GridLayout(2, 3));
            vehiclePanel.setPreferredSize(new Dimension(460, 70));
            vehiclePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10 ));
            vehiclePanel.setBorder(BorderFactory.createLineBorder(Color.black));
            JTextArea serial = new JTextArea();
            serial.setText(o.getSerial());
            serial.setFont(new Font("ARIAL BLACK", Font.BOLD, 18));
            vehiclePanel.add(serial);
            JTextArea model = new JTextArea();
            model.setText(o.getModel());
            model.setFont(new Font("ARIAL BLACK", Font.BOLD, 18));
            vehiclePanel.add(model);
            JTextArea color = new JTextArea();
            color.setText(o.getColor());
            color.setFont(new Font("ARIAL BLACK", Font.BOLD, 18));
            vehiclePanel.add(color);
            JTextArea prodYear = new JTextArea();
            prodYear.setText(String.valueOf(o.getProdYear()));
            prodYear.setFont(new Font("ARIAL BLACK", Font.BOLD, 18));
            vehiclePanel.add(prodYear);
            JButton delete = new JButton("Remove vehicle");
            delete.addActionListener(this);
            delete.putClientProperty("id", o.getID());
            vehiclePanel.add(delete);
            add(vehiclePanel);
        }

        JButton addCar = new JButton("Add a vehicle");
        addCar.setPreferredSize(new Dimension(150, 50));
        addCar.addActionListener(this);
        add(addCar);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Remove vehicle")) {
            JButton source = (JButton)e.getSource();
            controller.deleteVehicle((UUID)source.getClientProperty("id"));
        }
        if(e.getActionCommand().equals("Add a vehicle")) {
            controller.changeView(new AddVehicle(controller));
        }
    }
}
