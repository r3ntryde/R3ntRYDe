package org.r3ntryde;

import org.r3ntryde.data.facade.DataFacade;
import org.r3ntryde.data.persistance.facade.PersistanceFacade;
import org.r3ntryde.domain.facade.DomainFacade;
import org.r3ntryde.ui.controller.ModelViewController;

public class StartProgram {

    public static void main(String []args) {

        PersistanceFacade persistanceFacade = new PersistanceFacade();
        DataFacade dataFacade = new DataFacade(persistanceFacade);
        DomainFacade domainFacade = new DomainFacade((dataFacade));
        ModelViewController modelViewController = new ModelViewController(domainFacade);
        domainFacade.addObserver((modelViewController));
        persistanceFacade.addObserver(modelViewController);

        //domainFacade.resetSave();

    }
}
